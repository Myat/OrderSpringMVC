<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="footer, basic, centered, links" />
<title>All Products</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link href="resources/css/table.css" rel="stylesheet" type="text/css">

</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li> 
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> SIGN UP</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>

<div class = "container">
<h2>Product List</h2>

<a href="http://localhost:8080/OrderSpring/addproduct" class="btn btn-info" role="button">New</a>
<a href="http://localhost:8080/OrderSpring/downloadproduct" class="btn btn-info" role="button">Download CSV</a>
<a href="http://localhost:8080/OrderSpring/uploadcsv" class="btn btn-info" role="button">Upload CSV</a>
<br><br>
<div class="table-responsive">
	<table class = "table table-striped">
	<!-- <thead> -->
		<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Price</th>
				<th colspan="2">Actions</th>
		</tr>
	<!-- </thead> -->
	<!-- <tbody> -->
		<!-- inside loop create rows  -->
		<c:forEach var="product" items="${productList}">
			<tr>
				<td>${product.id}</td>
				<td>${product.name}</td>
				<td>${product.price}</td>
				<td><a href="http://localhost:8080/OrderSpring/editproduct?id=${product.id}">Edit</a></td>
				<td><a href="http://localhost:8080/OrderSpring/deleteproduct?id=${product.id}" onclick="return confirm('Are you sure?')">Delete</a></td>
			</tr>
		</c:forEach>
		<!-- end inside loop create rows  -->
	<!-- </tbody>	 -->
	</table>
</div>
<div id="pagination">
       <c:url value="/product" var="prev">
           <c:param name="page" value="${page-1}" />
       </c:url>
     <c:if test="${page > 1}">
        <ul  class = "pagination">  
       		<li><a href="<c:out value="${prev}" />" class="pn prev">Prev</a></li>
       	</ul>
       		</c:if>
       	
        <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
           <c:choose>
               <c:when test="${page == i.index}">
                   <ul class = "pagination"><li class="active"><a href="#">${i.index}</a></li></ul>
               </c:when>
               <c:otherwise>
                   <c:url value="/product" var="url">
                       <c:param name="page" value="${i.index}" />
                   </c:url>
                   <ul class = "pagination"><li><a href='<c:out value="${url}" />'>${i.index}</a></li></ul>
               </c:otherwise>
           </c:choose>
       </c:forEach>
       
       <c:url value="/product" var="next">
           <c:param name="page" value="${page + 1}" />
       </c:url>
       
       <c:if test="${page + 1 <= maxPages}">
           <ul  class = "pagination">
           		<li><a href='<c:out value="${next}" />' class="pn next">Next</a></li>
           </ul>
       </c:if>
     
   </div>
 </div>
 
 <footer class="footer-basic-centered">

			<p class="footer-company-motto">The company motto.</p>

			<p class="footer-links">
				<a href="#">Home</a>
				�
				<a href="#">Blog</a>
				�
				<a href="#">Pricing</a>
				�
				<a href="#">About</a>
				�
				<a href="#">Faq</a>
				�
				<a href="#">Contact</a>
			</p>

			<p class="footer-company-name">Company Name &copy; 2015</p>

		</footer>
</body>
</html>