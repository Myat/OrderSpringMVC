<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Add Product</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


<link href="resources/css/AddProductCSS.css" rel="stylesheet" type="text/css">
<style>
	.error { 
		color: red; font-weight: bold; 
	}
	
</style>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li> 
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>

<div class = "container">
<h2>Add Product</h2>
<br>
	<form:form class="form-horizontal" role="form" method="POST" action="product" commandName="product">
	
		<div class="form-group">
			<form:label class="control-label col-sm-2" path="name">Name:</form:label>			
			<div class="col-sm-10">
				<form:input path="name" class="form-control" placeholder="Enter Product Name"/>
				<form:errors path="name" cssClass="error"/>
			</div>
		</div>
		<div class="form-group">
			<form:label class="control-label col-sm-2" path="price">Price:</form:label>
			<div class="col-sm-10">				
				<form:input path="price" class="form-control" placeholder="Enter Price"/>
				<form:errors path="price" cssClass="error"/>
			</div>
		</div>
			
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">		
					<input type="submit" class="btn btn-primary btn-lg" value="Save" />
				</div>
			</div>

	</form:form>
</div>
</body>
</html>