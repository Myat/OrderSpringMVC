package orderexercise;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ProductController {
	@Autowired
	private ProductDAL productDal;
	List<Product> productList;
	
	/*@Autowired
	private UserDAL userDal;*/
	
	@RequestMapping(value = "/uploadcsv", method = RequestMethod.GET)
	public String showUploadPage(){
	     return "uploadCSV";
	}

	//@RequestMapping(value = { "/" , "/product**" }, method = RequestMethod.GET)
	
	/*@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(){
		//ModelAndView modelView = new ModelAndView("login", "user", new User());
	    return "redirect:/product";
	}*/
	
	
	
	/*@RequestMapping(value = "/userlogin")
	public ModelAndView login(@ModelAttribute("user") @Valid User user){
		
		if ( user.getName()!= null && user.getPassword()!= null) {
			if (userDal.getUser(user) != null) {
				ModelAndView modelView = new ModelAndView("product");
				modelView.addObject("username", user.getName());
			    return modelView;
			}				
		}
		
		ModelAndView modelView = new ModelAndView("login");
		modelView.addObject("user", user);
		modelView.addObject("loginfail", "Invalid user name or password! Please try again..");
	    return modelView;
	}*/
	
	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ModelAndView product(@RequestParam(required=false)Integer page, Principal principal) {

		System.out.println(".......... inside /product ............");
		
		productList = new ArrayList<>();
		try {
			productList = productDal.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}		
		
		PagedListHolder<Product> pagedListHolder = new PagedListHolder<>(productList);
	       pagedListHolder.setPageSize(5);       
	       ModelAndView modelView = new ModelAndView("product");
	        modelView.addObject("productList", productList);   
	       modelView.addObject("maxPages", pagedListHolder.getPageCount());
	      if (page == null || page < 1 || page > pagedListHolder.getPageCount())
	           page = 1;
	       modelView.addObject("page", page);
	       if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
	           pagedListHolder.setPage(0);
	           modelView.addObject("productList", pagedListHolder.getPageList());
	       } else if (page <= pagedListHolder.getPageCount()) {
	           pagedListHolder.setPage(page - 1);
	           modelView.addObject("productList", pagedListHolder.getPageList());
	       }
		return modelView;
	}
	
	@RequestMapping(value = "/addproduct", method = RequestMethod.GET)
	public ModelAndView addProduct() {

		 ModelAndView modelView = new ModelAndView("addproduct", "product", new Product());
	     return modelView;
	}
	
	@RequestMapping(value = "/downloadproduct", method = RequestMethod.GET)
	public void downloadproduct(HttpServletResponse response){
		//C:\Java\eclipse mars\SpringWorkSpace\OrderSpring\WebContent\download\product.csv
		//C:/Java/eclipse mars/SpringWorkSpace/OrderSpring/WebContent/download/product.csv
		String productFile = "C:/Java/eclipse mars/SpringWorkSpace/OrderSpring/WebContent/download/product.csv";
		File file = new File(productFile);
		try {
			FileWriter fw = new FileWriter(file);
			fw.append("Name");
			fw.append(',');
			fw.append("Price");
			fw.append('\n');
			for(Product product:productList){
				fw.append(product.getName());
				fw.append(',');
				fw.append(product.getPrice()+"");
				fw.append('\n');
			}
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	     try {
			InputStream is = new FileInputStream(file);
			response.setContentType("application/octet-stream");
			   response.setHeader("Content-Disposition", "attachment; filename=\""
		                + file.getName() + "\"");
		        // Read from the file and write into the response
		        OutputStream os = response.getOutputStream();
		        byte[] buffer = new byte[1024];
		        int len;
		        while ((len = is.read(buffer)) != -1) {
		            os.write(buffer, 0, len);
		        }
		        os.flush();
		        os.close();
		        is.close();
	     }
		 catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/product", method = RequestMethod.POST)
	   public String product(@Valid @ModelAttribute("product") Product product, BindingResult bindingResult,
	   ModelMap model) {
		
		System.out.println("...... inside /product POST .....");
		if(!bindingResult.hasErrors()){
			try {
				productDal.update(product);
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}	
			return "redirect:/product";
		}else{
			return "addproduct";
		}
			
	}
	
	@RequestMapping(value = "/editproduct", method = RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request,
			HttpServletResponse response)throws Exception {
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		Product product = findProductById(id); 
		 ModelAndView modelView = new ModelAndView("editproduct", "command",product);
	     return modelView;
	}
	
	@RequestMapping(value = "/deleteproduct", method = RequestMethod.GET)
	   public ModelAndView productdeleted(@ModelAttribute("SpringWeb")Product product, 
	   ModelMap model) {
		
		try {
			productDal.delete(product);
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
				
		return new ModelAndView("redirect:/product");
	
	}
	
	private Product findProductById(int id) {
		
		Product product = new Product();
		
		for (Product p : productList) {
			if(p.getId() == id) {
				product.setId(p.getId());
				product.setName(p.getName());
				product.setPrice(p.getPrice());
			}
		}
		
		return product;
	}

	/**
	 * Upload single file using Spring Controller
	 * @ResponseBody
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file) {

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name + ".csv");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				System.out.println("Server File Location="
						+ serverFile.getAbsolutePath());
				
				productList = crunchifyCSVtoArrayList(serverFile);
				
				/*for (int i=0; i<productList.size(); i++) {
					System.out.println("product before save " + productList.get(i).getName());
					productDal.add(productList.get(i));
				}*/
				
				for (Product product: productList) {
					System.out.println("product before save " + product.getName());
					productDal.add(product);
				}
				
				System.out.println("You successfully uploaded file = " + name);

				return "redirect:/product";
			} catch (Exception e) {
				return "redirect:/uploadcsv";
				//return "You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}
		
		
	}

	private List<Product> crunchifyCSVtoArrayList(File serverFile) {
		
		BufferedReader crunchifyBuffer = null;
		//List<String> crunchifyResult = new ArrayList<>();
		
		List<Product> productList = new ArrayList<>();		
		
		try {
			String crunchifyLine;
			crunchifyBuffer = new BufferedReader(new FileReader(serverFile));
			
			// How to read file in java line by line?
			while ((crunchifyLine = crunchifyBuffer.readLine()) != null) {
				System.out.println("Raw CSV data: " + crunchifyLine);				
				
				if (crunchifyLine != null && !crunchifyLine.contains("Name")) {
					String[] splitData = crunchifyLine.split("\\s*,\\s*");
					
					System.out.println(splitData[0]);
					System.out.println(splitData[1]);
					
					Product product = new Product();
					product.setName(splitData[0]);
					product.setPrice(Integer.valueOf(splitData[1]));
					
					productList.add(product);					
						
					/*for (int i = 0; i < splitData.length; i++) {
						if (!(splitData[i] == null) || !(splitData[i].length() == 0)) {
							
							crunchifyResult.add(splitData[i].trim());
						}
					}*/
				}	
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (crunchifyBuffer != null) crunchifyBuffer.close();
			} catch (IOException crunchifyException) {
				crunchifyException.printStackTrace();
			}
		}	
		System.out.println("size of productList " + productList.size());
		return productList;
	}
	
	

	
}


